# Secreto Helm ⛵️

Contains the Helm Chart for Deploying the [secreto-server](https://gitlab.com/k2511/secreto-server) and [secreto-client](https://gitlab.com/k2511/secreto-client).

## Requirements

To deploy the Secreto application the following is required:

- Kubernetes Cluster
- [Helm](https://helm.sh/)

## Usage

In order to install the Secreto Server and Client into your Kubernetes Cluster, you must
perform the following:

1. Build and push the secreto-client and secret-server to your container registry. 

**Note** You must edit the [.env](https://gitlab.com/k2511/secreto-client/-/blob/main/.env) file within the secreto-client before building.
It should point to the secreto-server ingress which will be your **http://[Ingress-Controller Load-Balancer IP]/[path in values.yaml](Values.yaml)**
for example http://34.67.207.202/server.

2. Clone the repository

```bash
$ git clone git@gitlab.com:k2511/secreto-helm.git

Cloning into 'secreto-helm'...
remote: Enumerating objects: 23, done.
remote: Counting objects: 100% (23/23), done.
remote: Compressing objects: 100% (21/21), done.
remote: Total 23 (delta 6), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (23/23), 6.14 KiB | 6.14 MiB/s, done.
Resolving deltas: 100% (6/6), done.
```

3. Setup Values.yaml as needed

4. Run the helm command

```bash
$ helm upgrade --install secreto . --values Values.yaml

Release "secreto" does not exist. Installing it now.
NAME: secreto
LAST DEPLOYED: Fri Apr  1 22:24:10 2022
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

5. Confirm the containers are running

```bash
$ kubectl get pods

NAME                              READY   STATUS    RESTARTS   AGE
secreto-client-69b87cf4-plrw9     1/1     Running   0          6m17s
secreto-server-5d77fc8dcb-5w9gw   1/1     Running   0          6m17s
```

## GKE Cluster Setup

Once the helm chart has been installed, in order to access the application you must perform
the following:

1. Install the Ingress Controller

```bash
$ helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
```

2. Get the Loadbalancer IP

```bash
$ kubectl get svc -n ingress-nginx

NAME                                 TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE
ingress-nginx-controller             LoadBalancer   10.108.7.101    34.67.207.202   80:32030/TCP,443:31613/TCP   20d
ingress-nginx-controller-admission   ClusterIP      10.108.11.232   <none>          443/TCP                      20d
```

3. Run the Helm commands as seen in the Usage section

4. Send a request to the server

```bash
$ curl 34.67.207.202/server/api/secreto/version

{"version":1}
```

5. Open up a browser and point towards the client-path (**http://34.67.207.202/client**)
You should see the client application correctly connected to server being seeing
secret cards described.

## Podman + Minikube Cluster

Some user may want to test using Podman and Minikube. This is possible, however there
are some limitations. For example, you will only be able to access the secreto-server
and will need to access it inside the minikube cluster.

In order to access your application, you must first perform the following steps:

1. Create a Podman Machine

```bash
$ podman machine init --cpus 2 --memory 2048 --disk-size 20

Extracting compressed file
Image resized.
Machine init complete

$ podman machine start

Starting machine "podman-machine-default"
INFO[0000] waiting for clients...
INFO[0000] new connection from  to /var/folders/mh/llxr_8d16t1dd2ypqfntvh6r0000gn/T/podman/qemu_podman-machine-default.sock
Waiting for VM ...
INFO[0028] Socket forward established: /Users/fern/.local/share/containers/podman/machine/podman-machine-default/podman.sock to /run/user/501/podman/podman.sock
...
Machine "podman-machine-default" started successfully

# Install Podman Mac Helper
$ sudo /opt/homebrew/Cellar/podman/4.0.2/bin/podman-mac-helper install

$ podman system connection default podman-machine-default-root
```

If using an M1 mac with podman and minikube, you must [configure your podman machine](https://edofic.com/posts/2021-09-12-podman-m1-amd64/) to be able to run these images:

```bash
$ podman machine ssh

Connecting to vm podman-machine-default. To close connection, use `~.` or `exit`
Warning: Permanently added '[localhost]:64147' (ED25519) to the list of known hosts.
Fedora CoreOS 35.20220305.dev.0
Tracker: https://github.com/coreos/fedora-coreos-tracker
Discuss: https://discussion.fedoraproject.org/tag/coreos

~ sudo -i

~ rpm-ostree install qemu-user-static

Checking out tree 5ce93ea... done
Enabled rpm-md repositories: fedora-cisco-openh264 fedora-modular updates-modular updates fedora updates-archive
Updating metadata for 'fedora-cisco-openh264'... done
Updating metadata for 'fedora-modular'... done
Updating metadata for 'updates-modular'... done
Updating metadata for 'updates'... done
Updating metadata for 'fedora'... done
Updating metadata for 'updates-archive'... done
Importing rpm-md... done
rpm-md repo 'fedora-cisco-openh264'; generated: 2021-09-21T18:07:30Z solvables: 4
rpm-md repo 'fedora-modular'; generated: 2021-10-26T05:06:57Z solvables: 1263
rpm-md repo 'updates-modular'; generated: 2022-04-02T03:06:19Z solvables: 1389
rpm-md repo 'updates'; generated: 2022-04-03T01:56:09Z solvables: 26224
rpm-md repo 'fedora'; generated: 2021-10-26T05:31:21Z solvables: 56722
rpm-md repo 'updates-archive'; generated: 2022-04-03T02:07:41Z solvables: 35475
Resolving dependencies... done
Will download: 9 packages (27.3 MB)
Downloading from 'updates'... done
Downloading from 'fedora'... done
Importing packages... done
Applying 1 override and 9 overlays
Processing packages... done
Running pre scripts... done
Running post scripts... done
Running posttrans scripts... done
Writing rpmdb... done
Writing OSTree commit... done
Staging deployment... done
Removed:
  moby-engine-20.10.12-1.fc35.aarch64
Added:
  ipxe-roms-qemu-20200823-7.git4bd064de.fc35.noarch
  mpdecimal-2.5.1-2.fc35.aarch64
  python-pip-wheel-21.2.3-4.fc35.noarch
  python-setuptools-wheel-57.4.0-1.fc35.noarch
  python-unversioned-command-3.10.4-1.fc35.noarch
  python3-3.10.4-1.fc35.aarch64
  python3-libs-3.10.4-1.fc35.aarch64
  qemu-common-2:6.1.0-14.fc35.aarch64
  qemu-user-static-2:6.1.0-14.fc35.aarch64
Changes queued for next boot. Run "systemctl reboot" to start a reboot

~ systemctl reboot

onnection to localhost closed by remote host.
Connection to localhost closed.
Error: exit status 255
```

Wait a couple of minutes for the podman machine to restart before proceeding.

2. Start Minikube

```bash
$ minikube start --driver=podman --container-runtime=cri-o --listen-address=0.0.0.0

😄  minikube v1.25.2 on Darwin 12.3 (arm64)
✨  Using the podman (experimental) driver based on user configuration
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
E0403 16:23:02.979952   40009 cache.go:203] Error downloading kic artifacts:  not yet implemented, see issue #8426
🔥  Creating podman container (CPUs=2, Memory=1953MB) ...
💡  minikube is not meant for production use. You are opening non-local traffic
❗  Listening to 0.0.0.0. This is not recommended and can cause a security vulnerability. Use at your own risk
🎁  Preparing Kubernetes v1.23.3 on CRI-O 1.22.1 ...
E0403 16:24:48.144932   40009 start.go:126] Unable to get host IP: RoutableHostIPFromInside is currently only implemented for linux
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔗  Configuring CNI (Container Networking Interface) ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

3. Verify minikube IP is 192.168.X.X

```bash
$ minikube ip

192.168.49.2
```

4. Enable Ingress

```bash
$ minikube addons enable ingress

💡  After the addon is enabled, please run "minikube tunnel" and your ingress resources would be available at "127.0.0.1"
    ▪ Using image k8s.gcr.io/ingress-nginx/controller:v1.1.1
    ▪ Using image k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.1.1
    ▪ Using image k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.1.1
🔎  Verifying ingress addon...
🌟  The 'ingress' addon is enabled
```

5. Run the Helm commands as seen in the Usage section

6. Create a tunnel and your ingress resources would be available at 192.168.X.X

```bash
$ minikube service -n ingress-nginx ingress-nginx-controller

🏃  Starting tunnel for service ingress-nginx-controller.
🎉  Opening service ingress-nginx/ingress-nginx-controller in default browser...
❗  Because you are using a Docker driver on darwin, the terminal needs to be open to run it.
```

**Note**: I haven't figured out the error above, which tells us we need to ssh into
minikube in order to access our ingress controller, which I know isn't practical. If you
know of a fix, create an issue or propose a fix. My solution in the future would be to allow
NodePort.

7. In a separate terminal, get the node port for the ingress-nginx-controller

```
$ kubectl get svc -n ingress-nginx

NAME                                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
ingress-nginx-controller             NodePort    10.107.109.145   <none>        80:31980/TCP,443:31915/TCP   27m
ingress-nginx-controller-admission   ClusterIP   10.111.94.26     <none>        443/TCP                      27m
```

8. ssh into minikube, and test server

```bash
$ minikube ssh

~ curl http://192.168.49.2:31980/server/api/secreto/version

{"version":1}
```

---

Project Avatar by <a href="https://unsplash.com/@lazycreekimages?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Michael Dziedzic</a> on <a href="https://unsplash.com/s/photos/secret?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
  
